<?php

namespace CodeArtisan\LaravelAcl;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use HasPermission;

    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() : BelongsToMany
    {
        return $this->belongsToMany(\App\User::class);
    }

    /**
     * @param string|int $role
     *
     * @return \CodeArtisan\LaravelAcl\Role|null
     */
    public static function findRole($role) : ?self
    {
        if (is_int($role)) {
            return self::find($role);
        }

        if (is_string($role)) {
            return self::whereName($role)->first();
        }

        return null;
    }
}
