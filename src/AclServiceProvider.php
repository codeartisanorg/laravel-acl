<?php

namespace CodeArtisan\LaravelAcl;

use Illuminate\Support\ServiceProvider;

class AclServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $timestamp = date('Y_m_d_His');
        $this->publishes([
            __DIR__.'/../database/migrations/create_acl_tables.php' => database_path("migrations/{$timestamp}_create_acl_tables.php"),
        ], 'codeartisan-acl');
    }
}
