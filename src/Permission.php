<?php

namespace CodeArtisan\LaravelAcl;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Permission extends Model
{
    use HasRole;

    protected $fillable = ['name', 'key'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() : BelongsToMany
    {
        return $this->belongsToMany(\App\User::class);
    }

    /**
     * @param int|string $permission
     *
     * @return \CodeArtisan\LaravelAcl\Permission|null
     */
    public static function findPermission($permission) : ?self
    {
        if (is_int($permission)) {
            return self::find($permission);
        }

        if (is_string($permission)) {
            return self::where('key', $permission)->first();
        }

        return null;
    }
}
