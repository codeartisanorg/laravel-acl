# Laravel ACL

Add roles to your Laravel application

## Installation

```
$ composer require codeartisan/laravel-acl
```

## Publish Migrations And Configurations

```
$ php artisan vendor:publish --tag codeartisan-acl
```

## Hos To Use It

### Enable Laravel ACL

```php
<?php
...
use CodeArtisan\LaravelAcl\HasRole;
...
class User extends Model
{
    use HasRole;
    ...
}
```

### Create New Role

```php
$role = Role::create([
    'name' => 'Administrator'
]);
```

### Assign User To Role

```php
$user = User::find(1);
$user->assignRole(2); // Assign by ID
$user->assignRole('Administrator'); // Assign by role name
```

### Revoke Role From User

```php
$user = User::find(1);
$user->revokeRole(2); // Revoke by ID
$user->revokeRole('Administrator'); // Revoke by role name
```

### Check If User Has A Given Role

```php
$user = User::find(1);
if ($user->hasRole('Admin')) {
    ...
}
```

### User Must Have At Least One Of These Roles

```php
$user = User::find(1);
if ($user->hasAnyRole(['Moderator', 'Admin'])) {
    ...
}
```

### User Must Have All These Roles

```php
$user = User::find(1);
if ($user->hasAllRoles(['Moderator', 'Admin'])) {
    ...
}
```

### Create New Permission
```php
$permission = Permission::create([
    'name' => 'New Permission',
    'key' => 'permission-key'
]);
```

### Grant User Direct Permission

```php
$user = User::find(1);
$user->grantPermission('permission-key');
```

### Grant Permission To A Role

```php
$role = Role::find(1);
$role->grantPermission('permission-key');
```

### Check If User Has Permission

```php
$user = User::find(1);
if ($user->hasPermission('permission-key')) {
    ...
}
```

### Check If User Has Direct Permission

```php
$user = User::find(1);
if ($user->hasDirectPermission('permission-key')) {
    ...
}
```

### Check If User Has Permission Via A Role

```php
$user = User::find(1);
if ($user->hasRolePermission('permission-key')) {
    ...
}
```

## Future Plans

* Add permissions

## Changelog

**2019-08-23**

* Initial release
